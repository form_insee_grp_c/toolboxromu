% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/search_folder_size.R
\name{search_folder_size}
\alias{search_folder_size}
\title{Title}
\usage{
search_folder_size(chemin, taille)
}
\arguments{
\item{chemin}{emplacement des fichiers}

\item{taille}{un entier, taille minimale des fichiers en bytes}
}
\value{
Une df avec la liste des fichiers de taille minimale
}
\description{
Description
}
\examples{
df_taille_min <- search_folder_size(system.file(package = "toolboxromu"), taille = 180)
}
