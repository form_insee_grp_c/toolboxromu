
<!-- README.md is generated from README.Rmd. Please edit that file -->

# toolboxromu

Ce package permet de lister les fichiers d’un chemin défini et d’en
récupérer la liste et la taille dans une dataframe de R.

## Installation

Procedure d’installation

``` r
library(remotes)
remotes::install_gitlab("form_insee_grp_c/toolboxromu")
#> Skipping install of 'toolboxromu' from a gitlab remote, the SHA1 (4797a27b) has not changed since last install.
#>   Use `force = TRUE` to force installation
```

## Exemples

Exemples d’utilisation des fonctions :

``` r
library(toolboxromu)
#get_one_file_siez() avec fichier existant dans le package
base_taille <- get_one_file_size(file = "rmd-vide-template.Rmd")

#get_all_file_size avec tous les fichiers qui sont dans le "/inst" du package
df_taille <- get_all_file_size(system.file(package = "toolboxromu"))

#search_folder_size avec tous les fichiers qui sont dans le "/inst" du package
#... dont la taille est superieure a 180 bytes
df_taille_min <- search_folder_size(system.file(package = "toolboxromu"), taille = 180)
```
